package com.system.converter;

import com.system.mapeamento.Pessoa;
import com.system.rn.PessoaRN;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;


@FacesConverter(value="tipoConverterDevedor")
public class TipoConverterDividaDev implements Converter {
    
    public List<Pessoa> lista; 
    
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
       
        
        
        if (submittedValue.trim().equals("")) { 
            return null;  
        } else {  
            try {  
                  
                  String str = submittedValue;
                 
                  PessoaRN rn = new PessoaRN();
                  this.lista = rn.listarSemFiltro();
  
                for (Pessoa p : this.lista) {  
                    if (String.valueOf(p.getPeNome()).equals(str)) {
                       
                        return p;  
                    }  
                }  
  
            } catch(NumberFormatException exception) {  
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tipo de Proposição inválido!"));  
            }  
        }  
  
        return null;  
    }
    
   
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
        if (value == null || value.equals("")) {  
            return null;  
        } else {  
            return String.valueOf(((Pessoa) value).getPeNome());
        }  
    }  
    
  

    
}
