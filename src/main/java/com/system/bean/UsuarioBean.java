/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.bean;

import com.system.mapeamento.Usuario;
import com.system.rn.UsuarioRN;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author wilson
 */
@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable {

    UsuarioRN usuarioRN;

    private Usuario usuarioSelecionado;
    private List<Usuario> usuarios;
    private Usuario usuario;

    public void salvar() {
        try {
            usuarioRN = new UsuarioRN();
            usuarioRN.salvar(usuario);
            this.usuarios = usuarioRN.listarSemFiltro();
            usuario = new Usuario();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Inclusão", "Usuário incluído com Sucesso!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (Exception e) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Ocorreu um erro na inclusão!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }
    
}
