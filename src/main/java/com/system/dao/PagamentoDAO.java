/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Pagamento;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author wilson
 */
public class PagamentoDAO implements PagamentoDAOListener {

    private Session sessao;

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    @Override
    public void salvar(Pagamento pgmt) {
        this.sessao.save(pgmt);
    }

    @Override
    public void alterar(Pagamento pgmt) {
        this.sessao.update(pgmt);
    }

    @Override
    public void excluir(Pagamento pgmt) {
        this.sessao.delete(pgmt);
    }

    @Override
    public List<Pagamento> listarSemFiltro() {
        return this.sessao.createCriteria(Pagamento.class).list();
    }

    @Override
    public Pagamento consultar(int matricula) {
        return (Pagamento) this.sessao.get(Pagamento.class, matricula);
    }

    @Override
    public Pagamento buscarPagamentosPorNome(String nome) {
        String hql = "select t from Aluno t where t.nome = :nomeA"; // ALTERAR CONSULTA
        Query consulta = this.sessao.createQuery(hql);
        consulta.setString("nomeA", nome); // ALTERAR CONSULTA
        return (Pagamento) consulta.uniqueResult();
    }

    @Override
    public List<Pagamento> listarPagamentosPorFaixa(int ini, int fim) {
        String hql = "select t from Aluno t where t.matricula >= :ini and t.matricula <= :fim"; // ALTERAR CONSULTA
        Query consulta = this.sessao.createQuery(hql);
        consulta.setInteger("ini", ini); // ALTERAR CONSULTA
        consulta.setInteger("fim", fim); // ALTERAR CONSULTA
        return (List<Pagamento>) consulta.list();
    }

    @Override
    public List<Pagamento> listarPagamentosPorLike(String part) {
        Criteria crit = this.sessao.createCriteria(Pagamento.class);
        crit.add(Restrictions.like("nome", "%" + part + "%")); // ALTERAR CONSULTA
        return (List<Pagamento>) crit.list();

    }
}
