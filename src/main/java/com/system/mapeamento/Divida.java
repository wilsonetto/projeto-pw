/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.mapeamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_divida")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Divida.findAll", query = "SELECT t FROM Divida t")
    , @NamedQuery(name = "Divida.findByDivId", query = "SELECT t FROM Divida t WHERE t.divId = :divId")
    , @NamedQuery(name = "Divida.findByDivDataatualizacao", query = "SELECT t FROM Divida t WHERE t.divDataatualizacao = :divDataatualizacao")
    , @NamedQuery(name = "Divida.findByDivValordadivida", query = "SELECT t FROM Divida t WHERE t.divValordadivida = :divValordadivida")})
public class Divida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "div_id")
    private Integer divId;
    @Column(name = "div_dataatualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date divDataatualizacao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "div_valordadivida")
    private BigDecimal divValordadivida;
    @OneToMany(mappedBy = "pagDivId")
    private List<Pagamento> PagamentoList;
    @JoinColumn(name = "div_credor", referencedColumnName = "pe_id")
    @ManyToOne
    private Pessoa divCredor;
    @JoinColumn(name = "div_devedor", referencedColumnName = "pe_id")
    @ManyToOne
    private Pessoa divDevedor;
    @Column(name = "div_status")
    private String divStatus;

    public Divida() {
    }

    public String getDivStatus() {
        return divStatus;
    }

    public void setDivStatus(String divStatus) {
        this.divStatus = divStatus;
    }

    public Divida(Integer divId) {
        this.divId = divId;
    }

    public Integer getDivId() {
        return divId;
    }

    public void setDivId(Integer divId) {
        this.divId = divId;
    }

    public Date getDivDataatualizacao() {
        return divDataatualizacao;
    }

    public void setDivDataatualizacao(Date divDataatualizacao) {
        this.divDataatualizacao = divDataatualizacao;
    }

    public BigDecimal getDivValordadivida() {
        return divValordadivida;
    }

    public void setDivValordadivida(BigDecimal divValordadivida) {
        this.divValordadivida = divValordadivida;
    }

    @XmlTransient
    public List<Pagamento> getPagamentoList() {
        return PagamentoList;
    }

    public void setPagamentoList(List<Pagamento> PagamentoList) {
        this.PagamentoList = PagamentoList;
    }

    public Pessoa getDivCredor() {
        return divCredor;
    }

    public void setDivCredor(Pessoa divCredor) {
        this.divCredor = divCredor;
    }

    public Pessoa getDivDevedor() {
        return divDevedor;
    }

    public void setDivDevedor(Pessoa divDevedor) {
        this.divDevedor = divDevedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (divId != null ? divId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Divida)) {
            return false;
        }
        Divida other = (Divida) object;
        if ((this.divId == null && other.divId != null) || (this.divId != null && !this.divId.equals(other.divId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.system.mapeamento.Divida[ divId=" + divId + " ]";
    }

}
