/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.bean;

import com.system.mapeamento.Pessoa;
import com.system.rn.PessoaRN;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author wilson
 */
@ManagedBean(name = "pessoaBean")
@SessionScoped

public class PessoaBean implements Serializable {

    PessoaRN pessoaRN;
    private Pessoa pessoaSelecionada;
    private List<Pessoa> pessoas;
    private Pessoa pessoa;

    public void popularDataTable() {
        pessoaRN = new PessoaRN();
        this.pessoas = pessoaRN.listarSemFiltro();
    }

    public PessoaBean() {
        pessoa = new Pessoa();
        popularDataTable();
    }

    public Pessoa getPessoaSelecionada() {
        return pessoaSelecionada;
    }

    public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
        this.pessoaSelecionada = pessoaSelecionada;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoa(List<Pessoa> ps) {
        this.pessoas = ps;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa ps) {
        this.pessoa = ps;
    }

    public void abrirDialog() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAl').show()");
        //requestContext.execute("PF('wdgdlgAl').show()"); no prime 5
    }

    public void abrirDialogAlt() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAlalt').show()");
    }

    public void salvar() {
        try {
           pessoaRN = new PessoaRN();
            pessoaRN.salvar(pessoa);
            this.pessoas = pessoaRN.listarSemFiltro();
            pessoa = new Pessoa();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Inclusão", "Pessoa incluída com Sucesso!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (Exception e) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Ocorreu um erro na inclusão!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        pessoaRN = new PessoaRN();
        pessoaRN.alterar(pessoaSelecionada);
        this.pessoas = pessoaRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alteração", "Pessoa alterada com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public void excluir() {
        pessoaRN = new PessoaRN();
        pessoaRN.excluir(pessoaSelecionada);
        this.pessoas = pessoaRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão", "Pessoa excluída com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

}

