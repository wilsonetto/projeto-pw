/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.rn;

import com.system.dao.PessoaDAOListener;
import com.system.mapeamento.Pessoa;
import com.system.util.FabricaDAO;
import java.util.List;

/**
 *
 * @author wilson
 */
public class PessoaRN {

    private PessoaDAOListener pessoaDAO;

    public PessoaRN() {
        this.pessoaDAO = FabricaDAO.criarPessoaDAO();
    }

    public void alterar(Pessoa ps) {
        this.pessoaDAO.alterar(ps);
    }

    public Pessoa buscarPagamentoPorNome(String nome) {
        return this.pessoaDAO.buscarPessoaPorNome(nome);
    }

    public Pessoa consultar(int matricula) {
        return this.pessoaDAO.consultar(matricula);
    }

    public void excluir(Pessoa ps) {
        this.pessoaDAO.excluir(ps);
    }

    public List<Pessoa> listarPagamentoPorLike(String part) {
        return this.pessoaDAO.listarPessoasPorLike(part);
    }

//    public List<Pessoa> listarPagamentoPorFaixa(int ini, int fim) {
//        return this.pessoaDAO.listarPessoasPorFaixa(ini, fim);
//    }
    public List<Pessoa> listarSemFiltro() {
        return this.pessoaDAO.listarSemFiltro();
    }

    public void salvar(Pessoa ps) {
        this.pessoaDAO.salvar(ps);
    }

    public void atualizar(Pessoa ps) {
        if (ps.getPeId() != null) {
            this.pessoaDAO.alterar(ps);
        } else {
            this.pessoaDAO.salvar(ps);

        }
    }
}
