/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.bean;

import com.system.mapeamento.Divida;
import com.system.mapeamento.Pessoa;
import com.system.rn.DividaRN;
import com.system.rn.PessoaRN;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
/**
 *
 * @author wilson
 */
@ManagedBean(name = "dividaBean")
@SessionScoped

public class DividaBean implements Serializable{

    DividaRN dividaRN;

    private Divida dividaSelecionada;
    private List<Divida> dividas;
    private Divida divida;
    private List<Pessoa> listaPessoa;
    private Pessoa devedor;

    public List<Pessoa> getListaPessoa() {
        return listaPessoa;
    }

    public void setListaPessoa(List<Pessoa> listaPessoa) {
        this.listaPessoa = listaPessoa;
    }

    public Pessoa getDevedor() {
        return devedor;
    }

    public void setDevedor(Pessoa devedor) {
        this.devedor = devedor;
    }

    public void popularDataTable() {
        dividaRN = new DividaRN();
        this.dividas = dividaRN.listarSemFiltro();
    }

    public DividaBean() {
        divida = new Divida();
        popularDataTable();
    }

    public Divida getDividaSelecionada() { // PagamentoSelecionadao
        return dividaSelecionada;
    }

    public void setDividaSelecionada(Divida dividaSelecionada) {
        this.dividaSelecionada = dividaSelecionada;
    }

    public List<Divida> getDividas() {
        return dividas;
    }

    public void setDividas(List<Divida> div) {
        this.dividas = div;
    }

    public Divida getDivida() {
        return divida;
    }

    public void setDivida(Divida div) {
        this.divida = div;
    }

    public void abrirDialog() {
        
        PessoaRN rn = new PessoaRN();
        listaPessoa = rn.listarSemFiltro();
        
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAl').show()");
        //requestContext.execute("PF('wdgdlgAl').show()"); no prime 5
    }

    public void abrirDialogAlt() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAlalt').show()");
    }

    public void salvar() {
        try {
            dividaRN = new DividaRN();
            dividaRN.salvar(divida);
            this.dividas = dividaRN.listarSemFiltro();
            divida = new Divida();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Inclusão", "Divida incluída com Sucesso!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (Exception e) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Ocorreu um erro na inclusão!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        dividaRN = new DividaRN();
        dividaRN.alterar(dividaSelecionada);
        this.divida = (Divida) dividaRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alteração", "Divida alterada com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public void excluir() {
        dividaRN = new DividaRN();
        dividaRN.excluir(dividaSelecionada);
        this.dividas = dividaRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão", "Divida excluída com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
}
