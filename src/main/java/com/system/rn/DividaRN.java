/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.rn;

import com.system.dao.DividaDAOListener;
import com.system.mapeamento.Divida;
import com.system.util.FabricaDAO;
import java.util.List;

/**
 *
 * @author wilson
 */
public class DividaRN {

    private DividaDAOListener dividaDAO;

    public DividaRN() {
        this.dividaDAO = FabricaDAO.criarDividaDAO();
    }

    public void alterar(Divida div) {
        this.dividaDAO.alterar(div);
    }

    public Divida buscarAlunoporNome(String nome) {
        return this.dividaDAO.buscarDividaPorNome(nome);
    }

    public Divida consultar(int matricula) {
        return this.dividaDAO.consultar(matricula);
    }

    public void excluir(Divida div) {
        this.dividaDAO.excluir(div);
    }

    public List<Divida> listarAlunosPorLike(String part) {
        return this.dividaDAO.listarDividasPorLike(part);
    }

    public List<Divida> listarAlunosporFaixa(int ini, int fim) {
        return this.dividaDAO.listarDividasPorFaixa(ini, fim);
    }

    public List<Divida> listarSemFiltro() {
        return this.dividaDAO.listarSemFiltro();
    }

    public void salvar(Divida div) {
        this.dividaDAO.salvar(div);
    }

    public void atualizar(Divida div) {
        if (div.getDivId() != null) {
            this.dividaDAO.alterar(div);
        } else {
            this.dividaDAO.salvar(div);

        }
    }
}