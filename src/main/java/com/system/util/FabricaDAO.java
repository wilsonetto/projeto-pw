/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.util;

import com.system.dao.DividaDAO;
import com.system.dao.DividaDAOListener;
import com.system.dao.PagamentoDAO;
import com.system.dao.PagamentoDAOListener;
import com.system.dao.PessoaDAO;
import com.system.dao.PessoaDAOListener;
import com.system.dao.UsuarioDAO;
import com.system.dao.UsuarioDAOListener;

/**
 *
 * @author wilson
 */
public class FabricaDAO {

    public static DividaDAOListener criarDividaDAO() {
        DividaDAO dividaDAO = new DividaDAO();
        dividaDAO.setSessao(HibernateUtil.getSessionFactory().getCurrentSession());
        return dividaDAO;
    }

    public static PagamentoDAOListener criarPagamentoDAO() {
        PagamentoDAO pagamentoDAO = new PagamentoDAO();
        pagamentoDAO.setSessao(HibernateUtil.getSessionFactory().getCurrentSession());
        return pagamentoDAO;
    }

    public static PessoaDAOListener criarPessoaDAO() {
        PessoaDAO pessoaDAO = new PessoaDAO();
        pessoaDAO.setSessao(HibernateUtil.getSessionFactory().getCurrentSession());
        return pessoaDAO;
    }

    public static UsuarioDAOListener criarUsuarioDAO() {
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        usuarioDAO.setSessao(HibernateUtil.getSessionFactory().getCurrentSession());
        return usuarioDAO;
    }
}
