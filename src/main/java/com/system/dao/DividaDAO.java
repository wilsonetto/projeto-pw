/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Divida;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author wilson
 */
public class DividaDAO implements DividaDAOListener {

    private Session sessao;

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    @Override
    public void salvar(Divida div) {
        this.sessao.save(div);
    }

    @Override
    public void alterar(Divida div) {
        this.sessao.update(div);
    }

    @Override
    public void excluir(Divida div) {
        this.sessao.delete(div);
    }

    @Override
    public List<Divida> listarSemFiltro() {
        return this.sessao.createCriteria(Divida.class).list();
    }

    @Override
    public Divida consultar(int id) {
        return (Divida) this.sessao.get(Divida.class, id);
    }

    @Override
    public Divida buscarDividaPorNome(String nome) {
        String hql = "select t from Aluno t where t.nome = :nomeA"; // ALTERAR A CONSULTA;
        Query consulta = this.sessao.createQuery(hql);
        consulta.setString("nomeA", nome); // ALTERAR 
        return (Divida) consulta.uniqueResult();
    }

    @Override
    public List<Divida> listarDividasPorFaixa(int ini, int fim) {
        String hql = "select t from Aluno t where t.matricula >= :ini and t.matricula <= :fim"; // ALTERAR A CONSULTA;
        Query consulta = this.sessao.createQuery(hql);
        consulta.setInteger("ini", ini); // ID 
        consulta.setInteger("fim", fim); // ID
        return (List<Divida>) consulta.list();
    }

    @Override
    public List<Divida> listarDividasPorLike(String part) {
        Criteria crit = this.sessao.createCriteria(Divida.class);
        crit.add(Restrictions.like("nome", "%" + part + "%")); // ALTERAR CONSULTA
        return (List<Divida>) crit.list();

    }
}
