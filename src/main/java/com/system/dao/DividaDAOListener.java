/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Divida;
import java.util.List;

/**
 *
 * @author wilson
 */
public interface DividaDAOListener {

    void alterar(Divida div);

    Divida buscarDividaPorNome(String nome);

    Divida consultar(int id);

    void excluir(Divida div);

    List<Divida> listarDividasPorFaixa(int ini, int fim);

    List<Divida> listarDividasPorLike(String part);

    List<Divida> listarSemFiltro();

    void salvar(Divida div);
    
}
