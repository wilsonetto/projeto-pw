/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.bean;

import com.system.mapeamento.Pagamento;
import com.system.rn.PagamentoRN;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author wilson
 */
@ManagedBean(name = "pagamentoBean")
@SessionScoped

public class PagamentoBean implements Serializable {

    PagamentoRN pagamentoRN;

    private Pagamento pagamentoSelecionado;
    private List<Pagamento> pagamentos;
    private Pagamento pagamento;

    public void popularDataTable() {
        pagamentoRN = new PagamentoRN();
        this.pagamentos = pagamentoRN.listarSemFiltro();
    }

    public PagamentoBean() {
        pagamento = new Pagamento();
        popularDataTable();
    }

    public Pagamento getPagamentoSelecionado() {
        return pagamentoSelecionado;
    }

    public void setPagamentoSelecionada(Pagamento pagamentoSelecionado) {
        this.pagamentoSelecionado= pagamentoSelecionado;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void setPagamento(List<Pagamento> pag) {
        this.pagamentos = pag;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pag) {
        this.pagamento = pag;
    }

    public void abrirDialog() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAl').show()");
        //requestContext.execute("PF('wdgdlgAl').show()"); no prime 5
    }

    public void abrirDialogAlt() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('wdgdlgAlalt').show()");
    }

    public void salvar() {
        try {
            pagamentoRN = new PagamentoRN();
            pagamentoRN.salvar(pagamento);
            this.pagamentos = pagamentoRN.listarSemFiltro();
            pagamento = new Pagamento();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Inclusão", "Pagamento incluída com Sucesso!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (Exception e) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Ocorreu um erro na inclusão!"); //
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        pagamentoRN = new PagamentoRN();
        pagamentoRN.alterar(pagamentoSelecionado);
        this.pagamentos = pagamentoRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alteração", "Pagamento alterada com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public void excluir() {
        pagamentoRN = new PagamentoRN();
        pagamentoRN.excluir(pagamentoSelecionado);
        this.pagamentos = pagamentoRN.listarSemFiltro();
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão", "Pagamento excluída com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
}
