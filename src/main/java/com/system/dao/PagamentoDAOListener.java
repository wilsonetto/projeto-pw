/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Pagamento;
import java.util.List;

/**
 *
 * @author wilson
 */
public interface PagamentoDAOListener {

    void alterar(Pagamento pgmt);

    Pagamento buscarPagamentosPorNome(String nome);

    Pagamento consultar(int matricula);

    void excluir(Pagamento pgmt);

    List<Pagamento> listarPagamentosPorFaixa(int ini, int fim);

    List<Pagamento> listarPagamentosPorLike(String part);

    List<Pagamento> listarSemFiltro();

    void salvar(Pagamento pgmt);
    
}
