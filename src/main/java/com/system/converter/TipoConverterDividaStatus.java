package com.system.converter;


import com.system.mapeamento.Divida;
import com.system.rn.DividaRN;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;


@FacesConverter(value="tipoConverterStatus")
public class TipoConverterDividaStatus implements Converter {
    
     public List<Divida> lista; 
    
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
       
        
        
        if (submittedValue.trim().equals("")) { 
            return null;  
        } else {  
            try {  
                  
                  String str = submittedValue;
                 
                    DividaRN rn = new DividaRN();
                  this.lista = rn.listarSemFiltro();
  
                for (Divida div : this.lista) {  
                    if (String.valueOf(div.getDivStatus()).equals(str)) {
                       
                        return div;  
                    }  
                }  
  
            } catch(NumberFormatException exception) {  
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tipo de Proposição inválido!"));  
            }  
        }  
  
        return null;  
    }
    
   
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {  
        if (value == null || value.equals("")) {  
            return null;  
        } else {  
            return String.valueOf(((Divida) value).getDivStatus());
        }  
    }  
    

    
}
