/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Pessoa;
import java.util.List;

/**
 *
 * @author wilson
 */
public interface PessoaDAOListener {

    void alterar(Pessoa ps);

    Pessoa buscarPessoaPorNome(String nome);

    Pessoa consultar(int matricula);

    void excluir(Pessoa ps);

    //    public List<Pessoa> listarAlunosporFaixa(int ini, int fim) {
    //        String hql = "select t from Aluno t where t.matricula >= :ini and t.matricula <= :fim";
    //        Query consulta = this.sessao.createQuery(hql);
    //        consulta.setInteger("ini", ini);
    //        consulta.setInteger("fim", fim);
    //        return (List<Pessoa>) consulta.list();
    //    }
    List<Pessoa> listarPessoasPorLike(String part);

    List<Pessoa> listarSemFiltro();

    void salvar(Pessoa ps);
    
}
