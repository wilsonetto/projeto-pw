/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.rn;

import com.system.dao.PagamentoDAOListener;
import com.system.mapeamento.Pagamento;
import com.system.util.FabricaDAO;
import java.util.List;
import static javax.management.Query.div;

/**
 *
 * @author wilson
 */
public class PagamentoRN {

    private PagamentoDAOListener pagamentoDAO;

    public PagamentoRN() {
        this.pagamentoDAO = FabricaDAO.criarPagamentoDAO();
    }

    public void alterar(Pagamento pgmt) {
        this.pagamentoDAO.alterar(pgmt);
    }

    public Pagamento buscarPagamentoPorNome(String nome) {
        return this.pagamentoDAO.buscarPagamentosPorNome(nome);
    }

    public Pagamento consultar(int matricula) {
        return this.pagamentoDAO.consultar(matricula);
    }

    public void excluir(Pagamento pgmt) {
        this.pagamentoDAO.excluir(pgmt);
    }

    public List<Pagamento> listarPagamentoPorLike(String part) {
        return this.pagamentoDAO.listarPagamentosPorLike(part);
    }

    public List<Pagamento> listarPagamentoPorFaixa(int ini, int fim) {
        return this.pagamentoDAO.listarPagamentosPorFaixa(ini, fim);
    }

    public List<Pagamento> listarSemFiltro() {
        return this.pagamentoDAO.listarSemFiltro();
    }

    public void salvar(Pagamento pgmt) {
        this.pagamentoDAO.salvar(pgmt);
    }

    public void atualizar(Pagamento pgmt) {
        if (pgmt.getPagId() != null) {
            this.pagamentoDAO.alterar(pgmt);
        } else {
            this.pagamentoDAO.salvar(pgmt);

        }
    }
}
