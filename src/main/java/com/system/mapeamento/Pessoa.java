/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.mapeamento;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_pessoa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pessoa.findAll", query = "SELECT t FROM Pessoa t")
    , @NamedQuery(name = "Pessoa.findByPeId", query = "SELECT t FROM Pessoa t WHERE t.peId = :peId")
    , @NamedQuery(name = "Pessoa.findByPeNome", query = "SELECT t FROM Pessoa t WHERE t.peNome = :peNome")
    , @NamedQuery(name = "Pessoa.findByPeEndereco", query = "SELECT t FROM Pessoa t WHERE t.peEndereco = :peEndereco")
    , @NamedQuery(name = "Pessoa.findByPeUf", query = "SELECT t FROM Pessoa t WHERE t.peUf = :peUf")
    , @NamedQuery(name = "Pessoa.findByPeTelefone", query = "SELECT t FROM Pessoa t WHERE t.peTelefone = :peTelefone")
    , @NamedQuery(name = "Pessoa.findByPeDocumento", query = "SELECT t FROM Pessoa t WHERE t.peDocumento = :peDocumento")
    , @NamedQuery(name = "Pessoa.findByPeEmail", query = "SELECT t FROM Pessoa t WHERE t.peEmail = :peEmail")})
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pe_id")
    private Integer peId;
    @Size(max = 100)
    @Column(name = "pe_nome")
    private String peNome;
    @Size(max = 100)
    @Column(name = "pe_endereco")
    private String peEndereco;
    @Size(max = 5)
    @Column(name = "pe_uf")
    private String peUf;
    @Size(max = 30)
    @Column(name = "pe_telefone")
    private String peTelefone;
    @Size(max = 50)
    @Column(name = "pe_documento")
    private String peDocumento;
    @Size(max = 100)
    @Column(name = "pe_email")
    private String peEmail;
    @OneToMany(mappedBy = "divCredor")
    private List<Divida> DividaList;
    @OneToMany(mappedBy = "divDevedor")
    private List<Divida> DividaList1;

    public Pessoa() {
    }

    public Pessoa(Integer peId) {
        this.peId = peId;
    }

    public Integer getPeId() {
        return peId;
    }

    public void setPeId(Integer peId) {
        this.peId = peId;
    }

    public String getPeNome() {
        return peNome;
    }

    public void setPeNome(String peNome) {
        this.peNome = peNome;
    }

    public String getPeEndereco() {
        return peEndereco;
    }

    public void setPeEndereco(String peEndereco) {
        this.peEndereco = peEndereco;
    }

    public String getPeUf() {
        return peUf;
    }

    public void setPeUf(String peUf) {
        this.peUf = peUf;
    }

    public String getPeTelefone() {
        return peTelefone;
    }

    public void setPeTelefone(String peTelefone) {
        this.peTelefone = peTelefone;
    }

    public String getPeDocumento() {
        return peDocumento;
    }

    public void setPeDocumento(String peDocumento) {
        this.peDocumento = peDocumento;
    }

    public String getPeEmail() {
        return peEmail;
    }

    public void setPeEmail(String peEmail) {
        this.peEmail = peEmail;
    }

    @XmlTransient
    public List<Divida> getDividaList() {
        return DividaList;
    }

    public void setDividaList(List<Divida> DividaList) {
        this.DividaList = DividaList;
    }

    @XmlTransient
    public List<Divida> getDividaList1() {
        return DividaList1;
    }

    public void setDividaList1(List<Divida> DividaList1) {
        this.DividaList1 = DividaList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (peId != null ? peId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.peId == null && other.peId != null) || (this.peId != null && !this.peId.equals(other.peId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return peNome;
        // "com.system.mapeamento.Pessoa[ peId=" + peId + " ]";
    }
    
}
