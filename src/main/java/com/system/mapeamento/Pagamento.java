/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.mapeamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_pagamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pagamento.findAll", query = "SELECT t FROM Pagamento t")
    , @NamedQuery(name = "Pagamento.findByPagId", query = "SELECT t FROM Pagamento t WHERE t.pagId = :pagId")
    , @NamedQuery(name = "Pagamento.findByPagDatapagamento", query = "SELECT t FROM Pagamento t WHERE t.pagDatapagamento = :pagDatapagamento")
    , @NamedQuery(name = "Pagamento.findByPagValorpago", query = "SELECT t FROM Pagamento t WHERE t.pagValorpago = :pagValorpago")})
public class Pagamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pag_id")
    private Integer pagId;
    @Column(name = "pag_datapagamento")
    @Temporal(TemporalType.DATE)
    private Date pagDatapagamento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "pag_valorpago")
    private BigDecimal pagValorpago;
    @JoinColumn(name = "pag_div_id", referencedColumnName = "div_id")
    @ManyToOne
    private Divida pagDivId;

    public Pagamento() {
    }

    public Pagamento(Integer pagId) {
        this.pagId = pagId;
    }

    public Integer getPagId() {
        return pagId;
    }

    public void setPagId(Integer pagId) {
        this.pagId = pagId;
    }

    public Date getPagDatapagamento() {
        return pagDatapagamento;
    }

    public void setPagDatapagamento(Date pagDatapagamento) {
        this.pagDatapagamento = pagDatapagamento;
    }

    public BigDecimal getPagValorpago() {
        return pagValorpago;
    }

    public void setPagValorpago(BigDecimal pagValorpago) {
        this.pagValorpago = pagValorpago;
    }

    public Divida getPagDivId() {
        return pagDivId;
    }

    public void setPagDivId(Divida pagDivId) {
        this.pagDivId = pagDivId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pagId != null ? pagId.hashCode() : 0);
        return hash;
    }
    
//    public List<Pagamento> listarTodosTiposDePagamento(){
//        
//    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pagamento)) {
            return false;
        }
        Pagamento other = (Pagamento) object;
        if ((this.pagId == null && other.pagId != null) || (this.pagId != null && !this.pagId.equals(other.pagId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.system.mapeamento.Pagamento[ pagId=" + pagId + " ]";
    }
    
}
