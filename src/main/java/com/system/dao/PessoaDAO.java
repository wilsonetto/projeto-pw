/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Pessoa;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author wilson
 */
public class PessoaDAO implements PessoaDAOListener {

    private Session sessao;

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    @Override
    public void salvar(Pessoa ps) {
        this.sessao.save(ps);
    }

    @Override
    public void alterar(Pessoa ps) {
        this.sessao.update(ps);
    }

    @Override
    public void excluir(Pessoa ps) {
        this.sessao.delete(ps);
    }

    @Override
    public List<Pessoa> listarSemFiltro() {
        return this.sessao.createCriteria(Pessoa.class).list();
    }

    @Override
    public Pessoa consultar(int pe_Id) {
        return (Pessoa) this.sessao.get(Pessoa.class, pe_Id);
    }

    @Override
    public Pessoa buscarPessoaPorNome(String nome) {
        String hql = "select t from Aluno t where t.nome = :nomeA"; // ALTERAR CONSULTA
        Query consulta = this.sessao.createQuery(hql);
        consulta.setString("nomeA", nome); // ALTERAR CONSULTA
        return (Pessoa) consulta.uniqueResult();
    }

//    public List<Pessoa> listarAlunosporFaixa(int ini, int fim) {
//        String hql = "select t from Aluno t where t.matricula >= :ini and t.matricula <= :fim";
//        Query consulta = this.sessao.createQuery(hql);
//        consulta.setInteger("ini", ini);
//        consulta.setInteger("fim", fim);
//        return (List<Pessoa>) consulta.list();
//    }

    @Override
    public List<Pessoa> listarPessoasPorLike(String part) {
        Criteria crit = this.sessao.createCriteria(Pessoa.class);
        crit.add(Restrictions.like("nome", "%" + part + "%")); // ALTERAR CONSULTA
        return (List<Pessoa>) crit.list();

    }
}
