/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.dao;

import com.system.mapeamento.Usuario;
import org.hibernate.Session;

/**
 *
 * @author wilson
 */
public class UsuarioDAO implements UsuarioDAOListener {

    private Session sessao;

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    @Override
    public void salvar(Usuario user) {
        this.sessao.save(user);
    }
    
//
//    public void alterar(Usuario user) {
//        this.sessao.update(user);
//    }
//
//    public void excluir(Usuario user) {
//        this.sessao.delete(user);
//    }

}
