/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.mapeamento;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT t FROM Usuario t")
    , @NamedQuery(name = "Usuario.findByUserId", query = "SELECT t FROM Usuario t WHERE t.userId = :userId")
    , @NamedQuery(name = "Usuario.findByUserNome", query = "SELECT t FROM Usuario t WHERE t.userNome = :userNome")
    , @NamedQuery(name = "Usuario.findByUserCargo", query = "SELECT t FROM Usuario t WHERE t.userCargo = :userCargo")
    , @NamedQuery(name = "Usuario.findByUserLogin", query = "SELECT t FROM Usuario t WHERE t.userLogin = :userLogin")
    , @NamedQuery(name = "Usuario.findByUserSenha", query = "SELECT t FROM Usuario t WHERE t.userSenha = :userSenha")
    , @NamedQuery(name = "Usuario.findByUserEmail", query = "SELECT t FROM Usuario t WHERE t.userEmail = :userEmail")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id")
    private Integer userId;
    @Size(max = 100)
    @Column(name = "user_nome")
    private String userNome;
    @Size(max = 30)
    @Column(name = "user_cargo")
    private String userCargo;
    @Size(max = 10)
    @Column(name = "user_login")
    private String userLogin;
    @Size(max = 6)
    @Column(name = "user_senha")
    private String userSenha;
    @Size(max = 100)
    @Column(name = "user_email")
    private String userEmail;

    public Usuario() {
    }

    public Usuario(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserNome() {
        return userNome;
    }

    public void setUserNome(String userNome) {
        this.userNome = userNome;
    }

    public String getUserCargo() {
        return userCargo;
    }

    public void setUserCargo(String userCargo) {
        this.userCargo = userCargo;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserSenha() {
        return userSenha;
    }

    public void setUserSenha(String userSenha) {
        this.userSenha = userSenha;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.system.mapeamento.Usuario[ userId=" + userId + " ]";
    }
    
}
